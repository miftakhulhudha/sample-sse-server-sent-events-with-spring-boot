package com.example.sse.server.model;

public class SensorMessage {

    private String message;

    public SensorMessage() {
    }

    public SensorMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
