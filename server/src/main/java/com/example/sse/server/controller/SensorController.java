package com.example.sse.server.controller;

import com.example.sse.server.model.SensorMessage;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;

import java.time.Duration;

@RestController
@RequestMapping("/sensors")
public class SensorController {

    private static ServerSentEvent<SensorMessage> apply(Long sequence) {
        return ServerSentEvent.<SensorMessage>builder()
                .id(String.valueOf(sequence))
                .event("notif")
                .data(new SensorMessage("data value : " + sequence))
                .retry(Duration.ofSeconds(10)) // delay time to retry or to reconnect
                .build();
    }

    @CrossOrigin
    @GetMapping("/stream")
    public Flux<ServerSentEvent<SensorMessage>> streamMessage(@RequestParam(value = "q", required = false, defaultValue = "no param") String q) {

        // sample with interval
        return Flux.interval(Duration.ofSeconds(1))
                .map(sequence -> ServerSentEvent.<SensorMessage>builder()
                        .id(String.valueOf(sequence))
                        .event("notif")
                        .data(new SensorMessage(q+" data value : " + sequence))
                        .retry(Duration.ofSeconds(5)) // delay time to retry or to reconnect
                        .build()
                );

        // sample with range
//        return Flux.range(1, 50000)
//                .map(Long::valueOf)
//                .map(SensorController::apply);
    }

}
