package com.example.sse.server.client.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class StreamController {

    @RequestMapping("/")
    public String index() {
        return "stream";
    }

}
