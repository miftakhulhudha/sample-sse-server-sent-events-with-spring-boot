package com.example.sse.server.client.repository.remote;

import com.example.sse.server.client.model.SensorMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

import java.time.LocalTime;

public class SensorClient {

    Logger log = LoggerFactory.getLogger(this.getClass());

    public void listenSensorMessage() {
        WebClient client = WebClient.create("http://localhost:8080/sensors");
        ParameterizedTypeReference<ServerSentEvent<SensorMessage>> type = new ParameterizedTypeReference<ServerSentEvent<SensorMessage>>() {
        };

        Flux<ServerSentEvent<SensorMessage>> eventStream = client.get()
                .uri("/stream")
                .retrieve()
                .bodyToFlux(type);

        eventStream.subscribe(
                content -> log.info("Time: {} - event name[{}], id [{}], content[{}] ", LocalTime.now(), content.event(), content.id(), content.data().getMessage()),
                error -> log.error("Error receiving sse : {}", error.getMessage()),
                () -> log.info("Completed")
        );

    }

}
