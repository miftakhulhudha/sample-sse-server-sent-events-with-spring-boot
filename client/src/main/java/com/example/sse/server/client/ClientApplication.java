package com.example.sse.server.client;

import com.example.sse.server.client.repository.remote.SensorClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClientApplication.class, args);

//		SensorClient client = new SensorClient();
//		client.listenSensorMessage();
	}

}
