function loadStream() {
    this. source = null;

    this.start = function() {
    console.log('start streaming data');
        var dataValue = document.getElementById("data");
        var id = parseInt((Math.random() * 100),10)
        this.source = new EventSource("http://localhost:8080/sensors/stream?q=menerima by id "+id);

        this.source.addEventListener("notif", function(event) {
            var sensorMessage = JSON.parse(event.data);
            dataValue.textContent = sensorMessage.message;
        });

        this.source.onError = function() {
            this.close();
        }
    }

    this.stop = function() {
        this.source.close();
    }
 }

 var stream = new loadStream();

 window.onload = function() {
    console.log('starting');
    stream.start();
 }

 window.onbeforeunload = function() {
    stream.stop();
 }